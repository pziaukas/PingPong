"""Interact with the client-facing command line."""
import click
from datetime import date, datetime
from jinja2 import Environment, FileSystemLoader
from pytz import timezone
from typing import Tuple
import ping_pong

Score = Tuple[int, int]


@click.command()
@click.option('--player', '-p', default='pz', show_default=True, help='Player code.')
@click.option('--opponent', '-o', default='pz', help='Opponent code.')
@click.option('--score', '-s', nargs=2, type=int, required=True, help='Player/opponent win counts.')
@click.option('--merge', is_flag=True, show_default=True, help='Merge to the latest tournament.')
def play(player: str, opponent: str, score: Score, merge: bool) -> None:
    """Add matches to the gamebook database."""
    try:
        if player == opponent:
            raise ValueError('Error: Cannot play against oneself!')
        if max(score) <= 0:
            raise ValueError('Error: No matches to play!')

        day = date.today()
        with ping_pong.GameBook() as gb:
            if player not in gb.players or opponent not in gb.players:
                raise ValueError('Error: Unknown player!')
            tournament = gb.get_tournament(day, merge)
            tournament.update(player, opponent, score[0])
            tournament.update(opponent, player, score[1])
        print(f'Updated the latest tournament:\n{tournament}')
    except ValueError as e:
        print(e)


@click.command()
@click.option('--elo-base', default=1000, show_default=True, help='Initial ELO score.')
@click.option('--elo-spread', default=300, show_default=True, help='ELO advantage for 10-1.')
@click.option('--days-ago', default=90, show_default=True, help='Show players active that many days ago.')
@click.option('--at-least', default=10, show_default=True, help='Show at least that many players.')
def build(elo_base: int, elo_spread: int, days_ago: int, at_least: int) -> None:
    """Build the leader-board HTML output."""
    print('Running the page builder...')
    gamebook = ping_pong.GameBook()
    leaderboard = ping_pong.LeaderBoard(elo_base, elo_spread)
    leaderboard.add_gamebook(gamebook)
    context = leaderboard.get_context(days_ago, at_least)
    clock = datetime.now(timezone('Europe/Vilnius')).strftime("%Y-%m-%d %H:%M:%S")

    environment = Environment(loader=FileSystemLoader('public'), trim_blocks=True)
    template = environment.get_template('index.html.jj')
    content = template.render(players=context, clock=clock)

    with open('public/index.html', 'w') as stream:
        stream.write(content)
    print('...completed!')

    gamebook.close()


if __name__ == '__main__':
    build()
