"""Generate the leader-board that contains individual player statistics."""
from datetime import date, timedelta
from typing import List
from ping_pong import GameBook

Context = List[dict]


class LeaderBoard:
    """Entity for the overview of individual player statistics.

    Attributes:
        elo_base: Default rating score for new players.
        elo_spread: Score difference for statistical advantage of 10-to-1.

    """

    def __init__(self, elo_base: int, elo_spread: int) -> None:
        """Initialize LeaderBoard with ELO base score and spread."""
        self.elo_base = elo_base
        self.elo_spread = elo_spread
        self.stats_by_player = {}

    def sync_statistics(self) -> None:
        """Synchronize accumulated rewards with the total score for all players."""
        for player_stats in self.stats_by_player.values():
            player_stats.elo = self.elo_base + int(sum(player_stats.rewards))

    def add_gamebook(self, gamebook: GameBook) -> None:
        """Import a gamebook into the leader-board."""
        for code, name in gamebook.players.items():
            self.stats_by_player.setdefault(code, PlayerStatistics(name, self.elo_base))
        for day, tournaments in gamebook.scoreboard.items():
            for tournament in tournaments:
                for winner, count_by_loser in tournament.items():
                    for loser, count in count_by_loser.items():
                        self.add_match(day, count, self.stats_by_player[winner], self.stats_by_player[loser])
                self.sync_statistics()

    def add_match(self, day: date, count: int, winner: 'PlayerStatistics', loser: 'PlayerStatistics') -> None:
        """Resolve a match (or multiple identical matches) between a pair of players."""
        unexpectedness = 1 / (1 + 10**((winner.elo - loser.elo) / self.elo_spread))
        # 1/10th of elo_spread for average players
        factor = 0.2 * self.elo_spread * self.elo_base / (winner.elo + loser.elo)
        reward = factor * unexpectedness

        for _ in range(count):
            winner.rewards.append(reward)
            loser.rewards.append(-reward)
        winner.last_active = day
        loser.last_active = day

    def get_context(self, days_ago: int, at_least: int) -> Context:
        """Summarize a filtered portion of the leader-board."""
        deadline = date.today() - timedelta(days=days_ago)
        context = []
        for player_stats in sorted(self.stats_by_player.values(), key=lambda x: x.last_active, reverse=True):
            if len(context) >= at_least and player_stats.last_active < deadline:
                break
            last_rewards = player_stats.rewards[-10:]
            entry = {'name': player_stats.name, 'points': player_stats.elo,
                     'trend': int(sum(last_rewards)),
                     'last_10': f'{sum(x >= 0 for x in last_rewards)}-{sum(x < 0 for x in last_rewards)}'}
            context.append(entry)

        context.sort(key=lambda x: x['points'], reverse=True)
        for k, entry in enumerate(context):
            entry['rank'] = k + 1
        return context


class PlayerStatistics:
    """Entity for the recap of an individual performance.

    Attributes:
        name: Name of a player.
        elo: Performance rating score.
        rewards: A list of rewards accumulated over time.
        last_active: Date of the most recently played match.

    """

    def __init__(self, name: str, elo: int) -> None:
        """Initialize PlayerStatistics with name and (initial) score."""
        self.name = name
        self.elo = elo
        self.rewards = []
        self.last_active = date(2000, 1, 1)
