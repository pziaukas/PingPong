"""Global configurations and imports for Ping Pong project."""

from pathlib import Path

__version__ = '1.1.1'
__gamebook__ = Path('db/gamebook.yaml')

from .cli import play
from .gamebook import GameBook
from .leaderboard import LeaderBoard

__all__ = [play, GameBook, LeaderBoard]
