"""Access and modify table tennis Tournaments within the GameBook database."""
from datetime import date
from typing import Dict
import yaml
from ping_pong import __gamebook__

Scores = Dict[str, Dict[str, int]]


class GameBook:
    """Entity that holds the table tennis players and played tournaments.

    Attributes:
        players: A collection of players by abbreviations.
        scoreboard: A collection of (list of) tournaments by dates.
        stream: An IO stream for data persistence.

    """

    def __init__(self, gb_stream=None) -> None:
        """Initialize GameBook with the database stream."""
        self.stream = gb_stream or open(__gamebook__, 'r+')
        self.players, self.scoreboard = yaml.safe_load_all(self.stream)

    def __enter__(self) -> 'GameBook':
        """Trivially open the instance within a context."""
        return self

    def __exit__(self, *args) -> None:
        """Gracefully save and close the instance within a context."""
        self.commit()
        self.close()

    def get_tournament(self, day: date, merge: bool) -> 'Tournament':
        """Get a tournament by date, or create a new one as needed."""
        if merge:
            scores = self.scoreboard[day][-1]
        else:
            scores = {}
            self.scoreboard.setdefault(day, []).append(scores)
        return Tournament(scores)

    def commit(self) -> None:
        """Persist file into a database stream."""
        self.stream.seek(0)
        yaml.dump_all([self.players, self.scoreboard], self.stream, allow_unicode=True)

    def close(self) -> None:
        """Close the database stream."""
        self.stream.close()


class Tournament:
    """Entity that holds scores from a single tournament.

    Attributes:
        _scores: A collection of losing player scores by winning players

    """

    def __init__(self, scores: Scores) -> None:
        """Initialize Tournament with scores data structure."""
        self._scores = scores

    def __str__(self) -> str:
        """Convert to human readable format."""
        return '\n'.join(f'{score}x: "{winner}" beats "{loser}"' for winner, loser_scores in self._scores.items()
                         for loser, score in loser_scores.items())

    def update(self, winner: str, loser: str, count: int) -> None:
        """Increase a score by the given count."""
        if count <= 0:
            return
        winner_sheet = self._scores.setdefault(winner, {})
        winner_sheet[loser] = winner_sheet.get(loser, 0) + count
