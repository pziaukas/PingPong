## Table Tennis Power Rankings

### Concept
Based on [Elo rating system](https://en.wikipedia.org/wiki/Elo_rating_system).

When Player A whose rating is $`R_A`$ defeats Player B whose rating is $`R_B`$, the unexpectedness of such an event is
```math
U = \frac{1}{1 + 10^{(R_A - R_B)/s}} .
```

A custom "jackpot" (formally a K-factor) is assigned to the match (1/10th of Elo spread for average players)
```math
K = \frac{2 s R}{10 (R_A + R_B)} .
```

The ratings are updated accordingly
```math
R_A^\prime = R_A + UK;
\ 
R_B^\prime = R_B - UK.
```

### Usage
```
Usage: ppong [OPTIONS]

  Adds matches to the gamebook.

Options:
  -p, --player TEXT       Player code.  [default: pz]
  -o, --opponent TEXT     Opponent code.
  -s, --score INTEGER...  Player/opponent win counts.  [required]
  --merge                 Merge to the latest tournament.  [default: False]
  --help                  Show this message and exit.
```



### Leaderboard
A static page is built in the `public/` directory.
```
Usage: build [OPTIONS]

  Builds the leaderboard output.

Options:
  --elo-base INTEGER    Initial ELO score.  [default: 1000]
  --elo-spread INTEGER  ELO advantage for 10-1.  [default: 300]
  --days-ago INTEGER    Show players active that many days ago.  [default: 90]
  --at-least INTEGER    Show at least that many players.  [default: 10]
  --help                Show this message and exit.
```



### Glossary
1. **Elo rating** - a method for calculating the relative skill levels. It's default starting score is $`R`$.
1. **Elo spread** - an arbitrary difference denoted $`s`$ (measured in Elo rating points) for players that are most likely to finish any given tournament with a score 10-1.
1. **Gamebook** - a database containing a list of players and a list of tournaments grouped by date.
1. **K-factor** - an amount of points that is distributed between a winner and a loser after a match.
1. **Leaderboard** - a dynamical Elo rating of players based on their historical performances. 
1. **Match** - an atomic event when one player wins against another player.
1. **Tournament** - a collection of matches.
