from pathlib import Path
import ping_pong
import unittest


class PingPongTest(unittest.TestCase):
    def test_package_version(self):
        self.assertEqual(ping_pong.__version__, '1.1.1')

    def test_gamebook_path(self):
        self.assertEqual(ping_pong.__gamebook__, Path('db/gamebook.yaml'))


if __name__ == '__main__':
    unittest.main()
