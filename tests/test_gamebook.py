from datetime import date
from io import StringIO
from textwrap import dedent
import unittest
import ping_pong


class GameBookTest(unittest.TestCase):
    def test_gamebook_scoreboard_size(self):
        self.assertEqual(len(self.gamebook.scoreboard), 2)
        self.assertEqual(len(self.gamebook.scoreboard[date(2018, 1, 1)]), 2)
        self.assertEqual(len(self.gamebook.scoreboard[date(2018, 1, 1)][0]), 2)
        self.assertEqual(len(self.gamebook.scoreboard[date(2018, 1, 1)][1]), 1)
        self.assertEqual(len(self.gamebook.scoreboard[date(2018, 1, 2)]), 1)
        self.assertEqual(len(self.gamebook.scoreboard[date(2018, 1, 2)][0]), 1)

    def test_gamebook_scoreboard_content(self):
        self.assertEqual(self.gamebook.scoreboard[date(2018, 1, 1)][0]['pz']['sz'], 111)

    def test_gamebook_tournament_size(self):
        tournament_12 = self.gamebook.get_tournament(date(2018, 1, 1), True)
        self.assertEqual(len(tournament_12._scores), 1)
        self.assertEqual(len(tournament_12._scores['pz']), 1)
        tournament_21 = self.gamebook.get_tournament(date(2018, 1, 2), True)
        self.assertEqual(len(tournament_21._scores), 1)
        self.assertEqual(len(tournament_21._scores['pz']), 1)
        tournament_empty1 = self.gamebook.get_tournament(date(2018, 1, 1), False)
        self.assertEqual(len(tournament_empty1._scores), 0)
        tournament_empty2 = self.gamebook.get_tournament(date(2018, 1, 3), False)
        self.assertEqual(len(tournament_empty2._scores), 0)

    def test_gamebook_tournament_content(self):
        tournament_12 = self.gamebook.get_tournament(date(2018, 1, 1), True)
        self.assertEqual(tournament_12._scores['pz']['sz'], 121)
        tournament_21 = self.gamebook.get_tournament(date(2018, 1, 2), True)
        self.assertEqual(tournament_21._scores['pz']['sz'], 211)

    def setUp(self):
        test_gamebook = '''
        pz: Mister Pranas
        sz: Missus Sima
        ---
        2018-01-01:
        - pz:
            sz: 111
          sz:
            pz: 112
        - pz:
            sz: 121
        2018-01-02:
        - pz:
            sz: 211
        '''
        self.gamebook = ping_pong.GameBook(StringIO(dedent(test_gamebook)))

    def tearDown(self):
        self.gamebook.close()


if __name__ == '__main__':
    unittest.main()
