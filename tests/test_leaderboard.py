from datetime import date
from io import StringIO
from random import randint
from textwrap import dedent
import unittest
import ping_pong


class LeaderBoardTest(unittest.TestCase):
    def test_leaderboard_empty(self):
        elo_base = randint(1000, 2000)
        elo_spread = randint(100, 500)
        lb = ping_pong.LeaderBoard(elo_base, elo_spread)
        self.assertEqual(lb.elo_base, elo_base)
        self.assertEqual(lb.elo_spread, elo_spread)
        self.assertEqual(len(lb.stats_by_player), 0)

    def test_leaderboard_default(self):
        lb = ping_pong.LeaderBoard(1000, 100)
        lb.add_gamebook(self.gamebook)
        self.assertEqual(len(lb.stats_by_player), 2)
        self.assertEqual(lb.stats_by_player['pz'].elo, lb.stats_by_player['sz'].elo)
        self.assertEqual(len(lb.stats_by_player['pz'].rewards), 4)
        self.assertEqual(len(lb.stats_by_player['sz'].rewards), 4)
        self.assertEqual(lb.stats_by_player['pz'].last_active, date(2018, 1, 1))
        self.assertEqual(lb.stats_by_player['sz'].last_active, date(2018, 1, 1))

    def test_leaderboard_advantage(self):
        lb = ping_pong.LeaderBoard(1000, 100)
        lb.add_gamebook(self.gamebook)
        lb.add_match(date(2018, 1, 2), 2, lb.stats_by_player['pz'], lb.stats_by_player['sz'])
        self.assertEqual(len(lb.stats_by_player['pz'].rewards), 6)
        self.assertEqual(len(lb.stats_by_player['sz'].rewards), 6)
        self.assertEqual(lb.stats_by_player['pz'].last_active, date(2018, 1, 2))
        self.assertEqual(lb.stats_by_player['sz'].last_active, date(2018, 1, 2))
        self.assertEqual(lb.stats_by_player['pz'].elo, lb.stats_by_player['sz'].elo)
        lb.sync_statistics()
        self.assertGreater(lb.stats_by_player['pz'].elo, lb.stats_by_player['sz'].elo)

    def test_leaderboard_context(self):
        lb = ping_pong.LeaderBoard(1000, 100)
        lb.add_gamebook(self.gamebook)
        lb.add_match(date(2018, 1, 2), 2, lb.stats_by_player['pz'], lb.stats_by_player['sz'])
        lb.sync_statistics()
        context = lb.get_context(7, 2)
        self.assertEqual(len(context), 2)
        self.assertEqual(context[0]['name'], 'Mister Pranas')
        self.assertEqual(context[0]['rank'], 1)
        self.assertEqual(context[0]['last_10'], '4-2')
        self.assertEqual(context[1]['name'], 'Missus Sima')
        self.assertEqual(context[1]['rank'], 2)
        self.assertEqual(context[1]['last_10'], '2-4')

    def setUp(self):
        test_gamebook = '''
        pz: Mister Pranas
        sz: Missus Sima
        ---
        2018-01-01:
        - pz:
            sz: 2
          sz:
            pz: 2
        '''
        self.gamebook = ping_pong.GameBook(StringIO(dedent(test_gamebook)))

    def tearDown(self):
        self.gamebook.close()


if __name__ == '__main__':
    unittest.main()
