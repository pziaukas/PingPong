.PHONY: environment build list test
$(VERBOSE).SILENT:

#################################################################################
# GLOBALS                                                                       #
#################################################################################

EXECUTOR = poetry run

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install (or update) Python environment
environment:
	poetry install

## Build the leader-board HTML
build:
	$(EXECUTOR) build

## Inspect the code style using flake8 and pydocstyle
lint:
	$(EXECUTOR) flake8 --max-line-length=120 --benchmark &&\
	$(EXECUTOR) pydocstyle

## Run the test suite using pytest
test:
	$(EXECUTOR) pytest
